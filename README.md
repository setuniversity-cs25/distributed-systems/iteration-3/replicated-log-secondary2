This is secondary2 server of "replicated log" system.
It runs on :8082 port

To receive all stored messages in secondary1 server, make GET request on localhost:8082/api/replicated-log/v1/message-memory

Sleep delay implemented before storing in memory process
