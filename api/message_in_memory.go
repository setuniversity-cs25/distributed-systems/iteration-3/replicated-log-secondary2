package api

import (
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"

	"gitlab.com/setuniversity-cs25/distributed-systems/replicated-log/models"
)

func messageInMemoryGetAll(c *gin.Context) {
	storedMessages := models.GetAllMessages()

	c.JSON(http.StatusOK, storedMessages)
}

func messageInMemoryCreate(c *gin.Context) {
	log.Println("secondary 2 storing message started")
	// Random status to return for experiments
	//
	// statuses := []int{http.StatusCreated, http.StatusInternalServerError}
	// rand.Shuffle(len(statuses), func(i, j int) {
	// 	statuses[i], statuses[j] = statuses[j], statuses[i]
	// })

	// if statuses[0] == http.StatusInternalServerError {
	// 	log.Println("Returning 500 error status for experiments")
	// 	c.JSON(statuses[0], nil)
	// 	return
	// }

	var message models.Message
	if err := c.ShouldBindJSON(&message); err != nil {
		log.Println("error occured on parsing in messageInMemoryCreate")
		c.Error(err)
		return
	}

	log.Println("secondary 2 sleep started")
	time.Sleep(5 * time.Second)
	log.Println("secondary 2 sleep finished")

	createdMessage := models.CreateMessage(message)
	log.Println("secondary 2 message stored")

	c.JSON(http.StatusCreated, createdMessage)
}
